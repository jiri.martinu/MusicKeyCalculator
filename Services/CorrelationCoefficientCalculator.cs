using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MusicKeyCalculator.Models;
using MusicKeyCalculator.Models.Automats;
using static MusicKeyCalculator.Models.ScaleSet;

namespace MusicKeyCalculator.Services
{
    public static class CorrelationCoefficientCalculator
    {
        public static IList<CorrelationCoefficienResult> CalculateForSets(IList<ScaleSet> sets)
        {
            IList<CorrelationCoefficienResult> results = new List<CorrelationCoefficienResult>();

            foreach (ScaleSet set in sets) {
                results.Add(CalculateForSet(set));
            }

            return results;
        }

        private static CorrelationCoefficienResult CalculateForSet(ScaleSet scaleSet)
        {
            CorrelationCoefficienResult result = new CorrelationCoefficienResult();

            result.BaseNote = scaleSet.BaseNote;
            result.Type = scaleSet.Type;
            result.Result = Calculate(scaleSet);

            return result;
        }

        private static decimal Calculate(ScaleSet scaleSet)
        {
            decimal top = (scaleSet.Values.Count() * SumXY(scaleSet) - SumX(scaleSet) * SumY(scaleSet));
            decimal bottomFirst = (decimal) Math.Pow((double) (scaleSet.Values.Count() * SumXinPower2(scaleSet) - SumXoutPower2(scaleSet)), (double) 1/2);
            decimal bottomLast = (decimal) Math.Pow((double) (scaleSet.Values.Count() * SumYinPower2(scaleSet) - SumYoutPower2(scaleSet)), (double) 1/2);

            return top / (bottomFirst * bottomLast);
        }

        private static decimal SumXY(ScaleSet scaleSet)
        {
            return scaleSet.getClearValues()
                .Sum((Tuple<decimal, long> values) => values.Item1 * values.Item2);
        }

        private static decimal SumX(ScaleSet scaleSet)
        {
            return scaleSet.getClearValues()
                .Sum((Tuple<decimal, long> values) => values.Item1);
        }

        private static long SumY(ScaleSet scaleSet)
        {
            return scaleSet.getClearValues()
                .Sum((Tuple<decimal, long> values) => values.Item2);
        }

        private static decimal SumXinPower2(ScaleSet scaleSet)
        {
            return scaleSet.getClearValues()
                .Sum((Tuple<decimal, long> values) => (decimal) Math.Pow((double) values.Item1, 2));
        }

        private static decimal SumYinPower2(ScaleSet scaleSet)
        {
            return scaleSet.getClearValues()
                .Sum((Tuple<decimal, long> values) => (decimal) Math.Pow((double) values.Item2, 2));
        }

        private static decimal SumXoutPower2(ScaleSet scaleSet)
        {
            return (decimal) Math.Pow((double) SumX(scaleSet), 2);
        }

        private static decimal SumYoutPower2(ScaleSet scaleSet)
        {
            return (decimal) Math.Pow((double) SumY(scaleSet), 2);
        }
    }
}