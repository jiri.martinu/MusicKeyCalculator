using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MusicKeyCalculator.Models;
using MusicKeyCalculator.Models.Automats;
using static MusicKeyCalculator.Models.ScaleSet;

namespace MusicKeyCalculator.Services
{
    public class MidiReader
    {
        public Automat Automat {get; set;} = AutomatInitializator.InitializeMidiReaderAutomat();

        public DurationsCounter DurationsCounter {get; set;} = new DurationsCounter();

        public IList<Note> activeNotes {get; set;} = new List<Note>();

        public long Ticks {get; set;} = 0;

        public void ReadAndCountMidi(string fileName)
        {
            if (File.Exists(fileName)) {
                Message message;

                using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open))) {
                    reader.ReadBytes(22);
                    byte input = new byte();

                    while (true) {
                        try {
                            input = reader.ReadByte();
                        } catch (EndOfStreamException) {
                            break;
                        }

                        message = Automat.ProcessInput(input);

                        if (message != null) {
                            ProcessMessage(message);
                        }
                    }

                    IList<ScaleSet> sets = ScaleSetsGenerator.Generate(DurationsCounter);
                    IList<CorrelationCoefficienResult> results = CorrelationCoefficientCalculator.CalculateForSets(sets);
                    CorrelationCoefficienResult result = results
                        .Where((CorrelationCoefficienResult res) => res.Result == results.Max((CorrelationCoefficienResult resul) => resul.Result))
                        .SingleOrDefault();

                    Console.WriteLine(result.BaseNote + " " + result.Type.ToString() + " " + result.Result.ToString());
                }
            }
        }

        private void ProcessMessage(Message message)
        {
            Ticks += message.GetTicks();

            message.StartTicks = Ticks;

            if (message.Type == Message.MessageType.Note) {
                Note note = message.GetNote();

                switch (note.Action) {
                    case Note.NoteAction.On:
                            activeNotes.Add(note);
                        break;
                    case Note.NoteAction.Off:
                            foreach (Note activeNote in activeNotes) {
                                if (activeNote.Name == note.Name) {
                                    DurationsCounter.Counts[note.Name] += Ticks - activeNote.StartTicks;
                                    activeNotes.Remove(activeNote);

                                    break;
                                }
                            }
                        break;
                }
            }
        }
    }
}