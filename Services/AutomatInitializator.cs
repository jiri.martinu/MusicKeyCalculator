using System;
using System.Collections.Generic;
using System.IO;
using MusicKeyCalculator.Models;
using MusicKeyCalculator.Models.Automats;

namespace MusicKeyCalculator.Services
{
    public static class AutomatInitializator
    {
        public static Automat InitializeMidiReaderAutomat()
        {
            State d = new State("D");

            State c = State.CreateState("C").AddEdge(Edge.AcceptAllEdge().SetEndState(d));
            State m = State.CreateState("M").AddEdge(Edge.AcceptAllEdge().SetEndState(d));
            State b = State.CreateState("B").AddEdge(Edge.AcceptAllEdge().SetEndState(c));
            State g = State.CreateState("G").AddEdge(Edge.AcceptAllEdge().SetEndState(d));
            State l = State.CreateState("L").AddEdge(Edge.AcceptAllEdge().SetEndState(g));
            State k = State.CreateState("K").AddEdge(Edge.AcceptAllEdge().SetEndState(l));
            State j = State.CreateState("J").AddEdge(Edge.AcceptAllEdge().SetEndState(k));
            State i = State.CreateState("I").AddEdge(Edge.AcceptAllEdge().SetEndState(j));
            State h = State.CreateState("H").AddEdge(Edge.AcceptAllEdge().SetEndState(i));

            State f = State.CreateState("F").AddEdges(new Edge[] {
                Edge.A01Edge().SetEndState(g),
                Edge.A02Edge().SetEndState(l),
                Edge.A03Edge().SetEndState(k),
                Edge.A04Edge().SetEndState(j),
                Edge.A05Edge().SetEndState(i),
                Edge.A06Edge().SetEndState(h),
            });

            State e = State.CreateState("E").AddEdges(new Edge[] {
                Edge.U2FAllEdge().SetEndState(f),
                Edge.A2FEdge().SetEndState(m)
            });

            State a = State.CreateState("A").AddEdges(new Edge[] {
                Edge.AFFEdge().SetEndState(e),
                Edge.A8090Edge().SetEndState(b)
            });

            a.AddEdge(Edge.U8090FFAllEdge().SetEndState(a));
            d.AddEdge(Edge.AcceptAllEdge().SetEndState(a));

            StateAction ChangeToBodyPhaseAndNoteTypeAction = (ref Message message, byte input) => {
                message.Phase = Message.MessagePhase.Body;
                message.Type = Message.MessageType.Note;

                message.AddByte(input);
            };

            StateAction ChangeToBodyPhaseAndEventTypeAction = (ref Message message, byte input) => {
                message.Phase = Message.MessagePhase.Body;
                message.Type = Message.MessageType.Event;

                message.AddByte(input);
            };

            b.Action = ChangeToBodyPhaseAndNoteTypeAction;
            e.Action = ChangeToBodyPhaseAndEventTypeAction;

            StateAction ChangeToEndPhaseAction = (ref Message message, byte input) => {
                message.AddByte(input);

                message.Phase = Message.MessagePhase.End;
            };

            d.Action = ChangeToEndPhaseAction;

            Automat automat = new Automat(a);

            return automat;
        }
    }
}