using System;
using System.Collections.Generic;
using System.Linq;
using MusicKeyCalculator.ExtensionMethods;
using MusicKeyCalculator.Models;
using static MusicKeyCalculator.Models.ScaleSet;
using static MusicKeyCalculator.Services.ProfilesGenerator;

namespace MusicKeyCalculator.Services
{
    public static class ScaleSetsGenerator
    {
        public static IList<ScaleSet> Generate(DurationsCounter durationsCounter)
        {
            IEnumerable<ScaleSet> result = new List<ScaleSet>();

            foreach (ScaleType scaleType in MyExtensions.GetEnumValues<ScaleType>()) {
                result = result.Concat(GenerateScaleTypeSets(durationsCounter, scaleType));
            }

            return result.ToList();
        }

        private static IList<ScaleSet> GenerateScaleTypeSets(DurationsCounter durationsCounter, ScaleType scaleType)
        {
            IList<string> rightScale;
            IEnumerable<Tuple<string, string>> mergedScale;
            IList<ScaleSet> scaleSets = new List<ScaleSet>();

            foreach (string scaleBaseKey in ProfilesGenerator.Profiles[scaleType].Keys) {
                rightScale = GenerateRightScale(scaleBaseKey, scaleType);

                mergedScale = ProfilesGenerator.Profiles[scaleType].Keys.Zip(rightScale, (first, second) => new Tuple<string, string>(first, second));

                scaleSets.Add(MapDurationCouterToScaleSet(durationsCounter, mergedScale, scaleBaseKey, scaleType));
            }

            return scaleSets;
        }

        private static IList<string> GenerateRightScale(string baseNote, ScaleType scaleType)
        {
            IList<string> scale = new List<string>(ProfilesGenerator.Profiles[scaleType].Keys);

            foreach (string note in ProfilesGenerator.Profiles[scaleType].Keys) {
                if (note == baseNote) {
                    break;
                }

                scale.Add(scale[0]);
                scale.RemoveAt(0);
            }

            return scale;
        }

        private static ScaleSet MapDurationCouterToScaleSet(
            DurationsCounter durationsCounter, 
            IEnumerable<Tuple<string, string>> setTuples,
            string baseNote, 
            ScaleType scaleType
        ) {
            ScaleSet scaleSet = new ScaleSet();

            scaleSet.BaseNote = baseNote;
            scaleSet.Type = scaleType;

            foreach (Tuple<string, string> tuple in setTuples) {
                scaleSet.Values.Add(
                    tuple, 
                    new Tuple<decimal, long>(
                        ProfilesGenerator.Profiles[scaleType][tuple.Item1], 
                        durationsCounter.Counts[tuple.Item2]
                    )
                );
            }

            return scaleSet;
        }
    }
}