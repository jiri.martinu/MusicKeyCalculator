using System.Collections.Generic;
using static MusicKeyCalculator.Models.ScaleSet;

namespace MusicKeyCalculator.Services
{
    public static class ProfilesGenerator
    {
        public static Dictionary<ScaleType, IDictionary<string, decimal>> Profiles = new Dictionary<ScaleType, IDictionary<string, decimal>>() {
            {ScaleType.Major, new Dictionary<string, decimal>() {
                {"C", (decimal) 6.35}, 
                {"C#", (decimal) 2.23}, 
                {"D", (decimal) 3.48}, 
                {"D#", (decimal) 2.33}, 
                {"E", (decimal) 4.38}, 
                {"F", (decimal) 4.09}, 
                {"F#", (decimal) 2.52}, 
                {"G", (decimal) 5.19}, 
                {"G#", (decimal) 2.39}, 
                {"A", (decimal) 3.66}, 
                {"A#", (decimal) 2.29},
                {"B", (decimal) 2.88}
            }},
            {ScaleType.Minor, new Dictionary<string, decimal>() {
                {"A", (decimal) 6.33}, 
                {"A#", (decimal) 2.68}, 
                {"B", (decimal) 3.52}, 
                {"C", (decimal) 5.38}, 
                {"C#", (decimal) 2.60}, 
                {"D", (decimal) 3.53}, 
                {"D#", (decimal) 2.54}, 
                {"E", (decimal) 4.75}, 
                {"F", (decimal) 3.98}, 
                {"F#", (decimal) 2.69}, 
                {"G", (decimal) 3.34}, 
                {"G#", (decimal) 3.17}
            }}
        };
    }
}