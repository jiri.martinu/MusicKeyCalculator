using System;
using System.Collections.Generic;
using System.Linq;
using static MusicKeyCalculator.Services.ProfilesGenerator;

namespace MusicKeyCalculator.Models
{
    public class ScaleSet
    {
        public enum ScaleType {Major, Minor};

        public string BaseNote {get; set;}

        public ScaleType Type {get; set;}

        public IDictionary<Tuple<string, string>, Tuple<decimal, long>> Values {get; set;} = new Dictionary<Tuple<string, string>, Tuple<decimal, long>>();

        public decimal Result {get; set;}

        public IEnumerable<Tuple<decimal, long>> getClearValues()
        {
            return Values.Select((KeyValuePair<Tuple<string, string>, Tuple<decimal, long>> keyValuePair) => 
                keyValuePair.Value
            );
        }
    }
}