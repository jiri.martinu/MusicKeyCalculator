using System.Collections.Generic;

namespace MusicKeyCalculator.Models.Automats
{
    public class Edge
    {
        public State EndState {get; set;}

        public bool AcceptAll {get; set;} = false;

        public IList<byte> AcceptableInputs {get; set;} = new List<byte>();

        public IList<byte> UnAcceptableInputs {get; set;} = new List<byte>();

        public bool IsAccepted(byte input)
        {
            if ((AcceptableInputs.Contains(input) || AcceptAll == true) && !UnAcceptableInputs.Contains(input)) {
                return true;
            }

            return false;
        }

        public Edge SetEndState(State endState)
        {
            EndState = endState;
            
            return this;
        }

        public static Edge U8090FFAllEdge()
        {
            return CreateEdge(new byte[] {}, new byte[] {0x80, 0x90, 0xFF}, true);
        }

        public static Edge A8090Edge()
        {
            return CreateEdge(new byte[] {0x80, 0x90}, new byte[] {}, false);
        }

        public static Edge AFFEdge()
        {
            return AcceptOneEdge((byte) 0xFF);
        }

        public static Edge A2FEdge()
        {
            return AcceptOneEdge((byte) 0x2F);
        }

        public static Edge U2FAllEdge()
        {
            return CreateEdge(new byte[] {}, new byte[] {0x2F}, true);
        }

        public static Edge A01Edge()
        {
            return AcceptOneEdge((byte) 0x01);
        }

        public static Edge A02Edge()
        {
            return AcceptOneEdge((byte) 0x02);
        }

        public static Edge A03Edge()
        {
            return AcceptOneEdge((byte) 0x03);
        }

        public static Edge A04Edge()
        {
            return AcceptOneEdge((byte) 0x04);
        }

        public static Edge A05Edge()
        {
            return AcceptOneEdge((byte) 0x05);
        }

        public static Edge A06Edge()
        {
            return AcceptOneEdge((byte) 0x06);
        }

        public static Edge AcceptAllEdge()
        {
            return CreateEdge(new byte[] {}, new byte[] {}, true);
        }

        public static Edge AcceptOneEdge(byte input)
        {
            return CreateEdge(new byte[] {input}, new byte[] {}, false);
        }

        public static Edge CreateEdge(byte[] acceptableInputs, byte[] unAcceptableInputs, bool acceptAll = false)
        {
            Edge edge = new Edge();

            edge.AcceptAll = acceptAll;

            foreach (byte input in acceptableInputs) {
                edge.AcceptableInputs.Add((byte) input);
            }

            foreach (byte input in unAcceptableInputs) {
                edge.UnAcceptableInputs.Add((byte) input);
            }

            return edge;
        }
    }
}