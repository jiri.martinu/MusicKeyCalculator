using System;
using System.Collections.Generic;
using static MusicKeyCalculator.Models.Message;

namespace MusicKeyCalculator.Models.Automats
{
    public class Automat
    {
        public Message Message = new Message();

        public State StartState {get; set;}

        public State State {get; set;}

        public Automat(State startState)
        {
            StartState = startState;
            State = StartState;
        }

        public Message ProcessInput(byte input)
        {
            State state = State.ProcessInput(input, ref Message);

            if (state != null && state != State) {
                State = state;

                if (Message.Phase == MessagePhase.End) {
                    Message tmp = Message;

                    Message = new Message();

                    return tmp;
                }
            }

            return null;
        }
    }
}