using System.Collections.Generic;
using static MusicKeyCalculator.Models.Automats.State;

namespace MusicKeyCalculator.Models.Automats
{
    public delegate void StateAction(ref Message message, byte input);

    public class State
    {
        public string Name {get; set;}

        public StateAction Action {get; set;} = (ref Message message, byte input) => {
            message.AddByte(input);
        };

        public IList<Edge> Edges {get; set;} = new List<Edge>();

        public State(string name)
        {
            Name = name;
        }

        public State ProcessInput(byte input, ref Message message)
        {
            foreach (Edge edge in Edges) {
                if (edge.IsAccepted(input)) {
                    edge.EndState.Action(ref message, input);

                    return edge.EndState;
                }
            }

            return null;
        }

        public static State CreateState(string name)
        {
            return new State(name);
        }

        public State AddEdges(Edge[] edges)
        {
            foreach (Edge edge in edges) {
                Edges.Add(edge);
            }

            return this;
        }

        public State AddEdge(Edge edge) 
        {
            Edges.Add(edge);

            return this;
        }
    }
}