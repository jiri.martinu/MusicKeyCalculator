using System;
using System.Collections.Generic;

namespace MusicKeyCalculator.Models
{
    public class Note
    {
        public enum NoteAction {On = 144, Off = 128};

        public static IList<string> Names {get; set;} = new List<string>() {
            "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
        };
        
        public string Name {get; set;}

        public NoteAction Action {get; set;}

        public int Pitch {get; set;}

        public int Volume {get; set;}

        public int Octave {get; set;}

        public long StartTicks {get; set;}

        public Note(int action, int pitch, int volume, long startTicks)
        {
            Action = (int) NoteAction.On == action && volume > 0 ? NoteAction.On : NoteAction.Off;
            Pitch = pitch;
            Volume = volume;

            Name = Names[Pitch % Names.Count];
            Octave = GetOctave();
            StartTicks = startTicks;
        }

        private int GetOctave()
        {
            return (int) Math.Round((decimal) (Pitch / Names.Count)) - 1;
        }
    }
}