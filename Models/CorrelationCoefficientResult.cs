using System.Collections.Generic;
using System.Linq;
using MusicKeyCalculator.ExtensionMethods;
using static MusicKeyCalculator.Models.ScaleSet;

namespace MusicKeyCalculator.Models
{
    public class CorrelationCoefficienResult
    {
        public string BaseNote;

        public ScaleType Type;

        public decimal Result;
    }
}