# Music Key Calculator

Calculates key (C# major, G minor etc.) for music in midi format using Krumhansl-Schmuckler key-finding algorithm.

## Usage 

    dotnet run [path to midi file]

## Resources

[http://rnhart.net/articles/key-finding/](http://rnhart.net/articles/key-finding/)